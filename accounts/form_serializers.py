from rest_framework import serializers
from .models import Currency, Account
import re


def raise_validation_error(error):
    raise serializers.ValidationError(
        error.get('message'),
        error.get('code')
    )


class AccountFormSerializer(serializers.Serializer):

    email = serializers.CharField(required=True)
    phone = serializers.CharField(required=False)

    def validate_phone(self, value):
        if re.match(r'^\+\d+$', value) is not None:
            return value
        else:
            raise serializers.ValidationError('wrong phone', 0)


class CurrencyFormSerializer(serializers.Serializer):

    name = serializers.CharField(required=True)
    symbol = serializers.CharField(required=True)


class BalanceFormSerializer(serializers.Serializer):

    currency = serializers.CharField(required=True)
    balance = serializers.FloatField(required=False, default=0, min_value=0)
    account = serializers.CharField(required=True)

    def validate_currency(self, value):
        try:
            currency = Currency.objects.get(symbol=value)
            return currency
        except Currency.DoesNotExist:
            raise serializers.ValidationError('currency symbol invalid')

    def validate_account(self, value):
        try:
            account = Account.objects.get(email=value)
            return account
        except Currency.DoesNotExist:
            raise serializers.ValidationError('account symbol invalid')
