from django.conf.urls import url
from .views import AccountsViewSet

urlpatterns = [
    url(r'^(?P<model_name>[\w]+)/(?P<pk>[\d]+)$', AccountsViewSet.as_view({'get': 'get_model'})),
    url(r'^(?P<model_name>[\w]+)$', AccountsViewSet.as_view({'get': 'get_list', 'post': 'post_model'})),


]
