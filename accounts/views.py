import functools

from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from django.db import IntegrityError

from accounts.exceptions import AccountException
from .models import Account, Currency, Balance
from .serializers import AccountSerializer, BalanceSerializer, CurrencySerializer
from .form_serializers import AccountFormSerializer, BalanceFormSerializer, CurrencyFormSerializer

# Create your views here.


def handle_error(method):
    @functools.wraps(method)
    def wrapper(self, request, *args, **kwargs):
        try:
            return method(self, request, *args, **kwargs)
        except AssertionError as e:
            try:
                code = getattr(e, 'code')
            except AccountException as e:
                return Response(dict(
                    error=True,
                    code=e.code,
                    message=e.message,
                ), status=status.HTTP_400_BAD_REQUEST)
            except AttributeError:
                code = None
                return Response(dict(
                    error=True,
                    code=code,
                    message=e.message
                ), status=status.HTTP_400_BAD_REQUEST)
        except IntegrityError as e:
            return Response(dict(
                error=True,
                message=e.args[0]
            ), status=status.HTTP_400_BAD_REQUEST)

    return wrapper


class ModelDescription:

    def __init__(self, model, serializer, form_serializer):
        self.model = model
        self.serializer = serializer
        self.form_serializer = form_serializer


class AccountsViewSet(viewsets.ViewSet):

    MODELS = {
        'account': ModelDescription(
            model=Account,
            serializer=AccountSerializer,
            form_serializer=AccountFormSerializer
        ),
        'currency': ModelDescription(
            model=Currency,
            serializer=CurrencySerializer,
            form_serializer=CurrencyFormSerializer
        ),
        'balance': ModelDescription(
            model=Balance,
            serializer=BalanceSerializer,
            form_serializer=BalanceFormSerializer
        ),
    }

    @handle_error
    @action(detail=True)
    def get_list(self, request, model_name):
        """
        method get models by model name and return list of instances
        """

        description = AccountsViewSet.MODELS.get(model_name)
        assert description, 'invalid model'
        model = description.model
        serializer = description.serializer
        model_list = model.objects.all()
        return Response([serializer(m).data for m in model_list])

    @handle_error
    @action(detail=True)
    def post_model(self, request, model_name):
        """
        get model by model name, create instance and return it
        """
        description = AccountsViewSet.MODELS.get(model_name)
        assert description, 'invalid model'
        form_serializer = description.form_serializer
        req = form_serializer(data=request.data)
        req.is_valid(raise_exception=True)
        model = description.model
        serializer = description.serializer
        pk = req.validated_data.get('id')
        if pk:
            try:
                m = model.objects.filter(id=pk)
                for key, value in dict(req.validated_data).items():
                    setattr(m, key, value)
            except model.DoesNotExist:
                raise AccountException(message='%s does not exists with id = %s' % (model_name, pk))
        else:
            m = model.objects.create(**dict(req.validated_data))
        m.save()
        return Response(serializer(m).data)

    @handle_error
    @action(detail=True)
    def get_model(self, request, model_name, pk):
        """
        get model instance by model name and pk
        """
        description = AccountsViewSet.MODELS.get(model_name)
        assert description, 'invalid model'
        model = description.model
        serializer = description.serializer
        try:
            m = model.objects.get(pk=pk)
        except model.DoesNotExist:
            raise AccountException(message='%s does not exists' % model_name)
        return Response(serializer(m).data)
