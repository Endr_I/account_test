from rest_framework import serializers

from accounts.models import Account, Currency, Balance


class AccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = ['id', 'email', 'phone', ]


class CurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = Currency
        fields = ['id', 'name', 'symbol', ]


class BalanceSerializer(serializers.ModelSerializer):

    currency = CurrencySerializer

    class Meta:
        model = Balance
        fields = ['id', 'balance', 'currency', ]
