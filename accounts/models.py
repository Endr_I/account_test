from django.db import models

# Create your models here.


class Account(models.Model):

    """
    account table
    """

    email = models.CharField(max_length=255, unique=True, null=False)
    phone = models.CharField(max_length=20, default=None, null=True)


class Currency(models.Model):

    """
    currency table
    it could contain rate to any default currency
    if there is many currencies to convert so new table Rates should be created
    """

    name = models.CharField(max_length=255, unique=True, null=False)
    symbol = models.CharField(max_length=10, unique=True, null=False)
    # rate = models.FloatField(default=0)


class Balance(models.Model):

    """
     creates balance table for users with different currencies
     it's related with currency and account and don't let them be deleted
      """

    balance = models.FloatField(default=0)
    account = models.ForeignKey(Account, null=False, on_delete=models.PROTECT)
    currency = models.ForeignKey(Currency, null=False, on_delete=models.PROTECT)
