# account_test

1. Create environment and activate it
2. Install requirements with
    ```bash
    pip install -r requirements.txt
    ```
3. Run server with
    ```bash
   python manage.py runserver
   ```
4. To get models list request `GET http://localhost:8000/accounts/<model_name>`. To get model `GET http://localhost:8000/accounts/<model_name>/<id>`. To edit or create model request `POST http://localhost:8000/accounts/<model_name>/<id>`

List of model_name: account, currency, balance  

